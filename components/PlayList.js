import { View } from "react-native";
import PlayUnit from "./PlayUnit";
import { useEffect, useState } from "react";
import { Playerapi } from '../api'

export default function Playlist() {
    let playerapi = Playerapi.getInstance();
    [currentUnit, setCurrentUnit] = useState({
        widget: 'image',
        url: "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
        duration: 10000,
        code: '1234',
        fit: 'contain'
    });
    useEffect(() => {

        let playloop_ctl = 0;

        async function playLoop() {
            let nextunit = await playerapi.getNextUnit();
            console.log(nextunit);
            clearTimeout(playloop_ctl);
            playloop_ctl = setTimeout(playLoop, nextunit.duration);
            setCurrentUnit(nextunit);
        }
        playloop_ctl = setTimeout(playLoop, 0);
        return () => {
            clearTimeout(playloop_ctl);
        }
    }, [])

    return (
        <View>
            <PlayUnit widget={currentUnit.widget} src={currentUnit.url} object_fit={currentUnit.fit} />
        </View>
    );
}