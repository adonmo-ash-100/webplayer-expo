import { View, StyleSheet, Image, Dimensions } from 'react-native';
// import { Image } from 'expo-image'

const PlaceHolderImage = require('../assets/icon.png');
export default function ImageWidget({ src, object_fit }) {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    return (
        <View style={styles.container}>
            <Image
                source={{ uri: src, headers: { 'Accept': 'image/*' } }}
                transition={1000}
                style={{ width: windowWidth, height: windowHeight, objectFit: object_fit }}
            />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

});