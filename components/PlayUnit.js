import { View, Text, StyleSheet, Dimensions } from "react-native";
import VideoWidget from "./VIdeoWidget";
import ImageWidget from "./ImageWidget";

export default function PlayUnit({ widget, src, object_fit }) {
    let _widget;
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    if (widget == "video") {
        _widget = <VideoWidget src={src} object_fit={object_fit} />
    }
    else if (widget == "image") {
        _widget = <ImageWidget src={src} object_fit={object_fit} />
    }
    else {
        _widget = <View style={{ alignContent: 'center', justifyContent: 'center', alignItems: 'center', width: windowWidth, height: windowHeight }}><Text>Unknown widget</Text></View>
    }
    return (
        <View style={styles.container}>
            {_widget}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },

});
