import { StyleSheet, View, Text, Button, Dimensions } from "react-native";
import { VideoView, useVideoPlayer } from "expo-video";
import { useEffect, useRef } from "react";
import { WebView } from 'react-native-webview';

const PlaceHolderVideo = require('../assets/adonmo_001.mp4')
export default function VideoWidget({ src, object_fit }) {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const ref = useRef(null);
    const player = useVideoPlayer(src, player => {
        player.loop = true;
        player.play();
    });
    useEffect(() => {
        return () => {

            player.release();
        }
    }, [])
    return (
        <View style={styles.contentContainer}>
            <VideoView ref={ref} style={{ width: windowWidth, height: windowHeight }} player={player} allowsFullscreen contentFit={object_fit} object_fit={object_fit} ></VideoView>
            <Text>hi</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
