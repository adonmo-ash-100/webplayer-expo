import { StyleSheet, Text, View, Image } from 'react-native';
import Playlist from './components/PlayList';

const PlaceholderImage = require('./assets/icon.png');
export default function App() {
  return (
    <View style={styles.container}>
      <Playlist />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    alignItems: 'center',
  }
});
