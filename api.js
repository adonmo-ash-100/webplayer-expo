export class Playerapi {
    static instance;
    playerState;
    API_ENDPOINT = "";
    constructor() {
        this.playerState = {
            'currentUnitIndex': 0,
            'activePlaylist': this.fallbackPlaylist(),
        }
    }

    static getInstance() {
        if (!Playerapi.instance) {
            Playerapi.instance = new Playerapi();
        }
        return Playerapi.instance;
    }

    fallbackPlaylist() {
        // Make sure there is atleast one unit in the playlist
        return {
            "id": "1",
            "units": [
                {
                    "code": "unit1",
                    "widget": "video",
                    "url": "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
                    "duration": 7500,
                    "fit": "contain",
                },
                {
                    "code": "unit2",
                    "widget": "image",
                    "url": "https://picsum.photos/250?image=9",
                    "duration": 4000,
                    "fit": "contain",
                },
                {
                    "code": "unit2",
                    "widget": "un",
                    "url": "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
                    "duration": 4000,
                    "fit": "contain",
                },
            ],
        };
    }

    async getPlaylist() {
        let api_url = this.API_ENDPOINT;
        console.log('fetching');
        return this.fallbackPlaylist();
    }

    async getNextUnit() {
        if (!this.playerState.activePlaylist || !this.playerState.activePlaylist.units.length) {
            this.playerState.activePlaylist = this.fallbackPlaylist();
        }
        let nunits = this.playerState.activePlaylist.units.length;

        this.playerState.currentUnitIndex = ((this.playerState.currentUnitIndex + 1) % nunits) || 0;
        return this.playerState.activePlaylist.units[this.playerState.currentUnitIndex];
    }


}